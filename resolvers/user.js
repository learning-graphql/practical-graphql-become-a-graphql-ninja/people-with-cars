const jwt = require('jsonwebtoken');
const { GraphQLScalarType } = require('graphql/type');
const cloudinary = require('cloudinary').v2;

cloudinary.config({
  cloud_name: process.env.CLOUD_NAME,
  api_key: process.env.API_KEY,
  api_secret: process.env.API_SECRET,
});

const createToken = (user, secret, expiresIn) => {
  const { id, name, username } = user;
  return jwt.sign({ id, name, username }, secret, { expiresIn });
};

const userResolvers = {
  Query: {
    users: (parent, args, { models }) => models.User.findAll(),
    user: (parent, { id }, { models }) => models.User.findByPk(id),
    me: (parent, args, { me }) => me,
  },
  Mutation: {
    makeUser: (parent, { name }, { models }) => {
      const user = {
        name,
      };
      return models.User.create(user);
    },
    removeUser: (parent, { id }, { models }) => models.User.destroy({
      where: {
        id,
      },
    }),
    register: async (parent, { name, username, password }, { models }) => {
      const user = {
        name,
        username,
        password,
      };
      const registeredUser = await models.User.create(user);
      try {
        return typeof registeredUser.id === 'number';
      } catch (e) {
        console.error(e);
        return false;
      }
    },
    login: async (parent, { username, password }, { models, secret }) => {
      const user = await models.User.findOne({ where: { username }});
      const validPassword = user && await user.validatePassword(password);
      if (!validPassword) {
        throw new Error("Invalid credentials");
      }

      return {
        token: createToken(user, secret, '30m'),
      };
    },
    uploadImage: async (parent, { id, filename }, { models, me }) => {
      // if (!me) {
      //   throw new Error('Not authenticated!');
      // }
      const path = require('path');
      const mainDir = path.dirname(require.main.filename);
      filename = `${mainDir}/uploads/${filename}`;
      try {
        const photo = await cloudinary.uploader.upload(filename);
        await models.User.update({
          photo: `${photo.public_id}.${photo.format}`,
        }, {
          // where: { username: me.username },
          where: { id: id },
        });
        return `${photo.public_id}.${photo.format}`;
      } catch (e) {
        console.error(e);
        throw new Error(e);
      }
    },
  },
  User: {
    cars: (parent, args, { models }) => models.Car.findAll({
      where: {
        userId: parent.id,
      },
    }),
    photo: (parent, { options }) => {
      let url = cloudinary.url(parent.photo);
      if (options) {
        // width: Int, q_auto: Boolean, f_auto: Boolean, face: 'face'
        const [ width, q_auto, f_auto, face ] = options;
        const cloudinaryOptions = {
          ...(q_auto === 'true' && { quality: 'auto' }),
          ...(f_auto === 'true' && { fetch_format: 'auto' }),
          ...(face && { crop: 'thumb', gravity: 'face' }),
          width,
          secure: true,
        };
        url = cloudinary.url(parent.photo, cloudinaryOptions);
      }
      return url;
    },
  },
  CloudinaryOptions: new GraphQLScalarType({
    name: 'CloudinaryOptions',
    parseValue(value) {
      return value;
    },
    serialize(value) {
      return value;
    },
    parseLiteral(ast) {
      return ast.value.split(',');
    },
  }),
};

module.exports = userResolvers;
