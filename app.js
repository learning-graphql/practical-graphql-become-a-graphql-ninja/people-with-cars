require('dotenv').config();
const express = require('express');
const app = express();
const { ApolloServer } = require('apollo-server-express');
const cors = require('cors');
const multer = require('multer');
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'uploads');
  },
  filename: function (req, file, cb) {
    cb(null, file.originalname);
  },
});
const upload = multer({ storage });

const models = require('./models');
const typeDefs = require('./typeDefs');
const resolvers = require('./resolvers');

const jwt = require('jsonwebtoken');

const routes = require('./routes');

const getLoggedInUser = req => {
  const token = req.headers['x-auth-token'];
  if (token) {
    try {
      return jwt.verify(token, process.env.JWT_SECRET);
    } catch (e) {
      console.error(e);
      throw new Error('Session expired');
    }
  }
};

const startServer = async () => {
  const server = new ApolloServer({
    typeDefs,
    resolvers,
    context: ({ req }) => ({
      models,
      secret: process.env.JWT_SECRET,
      me : getLoggedInUser(req),
    }),
  });
  await server.start();
  server.applyMiddleware({ app });
};

startServer().then(() => {
  app.use(cors());

  app.set('view engine', 'pug');
  app.set('views', `${__dirname}/public`);

  app.get('/', routes.index);
  app.get('/user/:id', routes.userInfo);
  app.post('/upload', upload.fields([{ name: 'file' }, { name: 'id' }]), routes.upload);

  app.listen(3000, () => console.info('Apollo GraphQL server is running on port 3000'));
}).catch(err => console.error('Failed to start the server', err));
