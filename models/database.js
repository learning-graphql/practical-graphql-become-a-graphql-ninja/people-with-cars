const { Sequelize } = require('sequelize');

const sequelize = new Sequelize('usersCars', 'postgres', 'postgres', {
  dialect: 'postgres',
  define: {
    timestamps: false,
  },
});

module.exports = {
  sequelize,
};
