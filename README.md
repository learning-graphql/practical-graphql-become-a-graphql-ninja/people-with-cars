# Sample application to learn GraphQL


The server application uses environment variables, which can be defined in `.env` file.

Application uses following environment variables:
- `JWT_SECRET` - secret string, which is used to encode JWT token
- `CLOUD_NAME` - your Cloud Name on Cloudinary
- `API_KEY` - API key for Cloudinary
- `API_SECRET` - API secret for Cloudinary
