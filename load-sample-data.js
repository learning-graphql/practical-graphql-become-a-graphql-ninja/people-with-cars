const { sequelize } = require('./models/database');
const models = require('./models');

const createData = async () => {
  await models.User.create({
    name: 'Edu',
    username: 'edu',
    password: 'test1',
    cars: [{
      make: 'Mercedes',
      model: 'GLA240',
      colour: 'Black',
    }],
  }, {
    include: [models.Car],
  });

  await models.User.create({
    name: 'Janna',
    username: 'janna',
    password: 'test2',
  });

  await models.User.create({
    name: 'Arno',
    username: 'arno',
    password: 'test3',
  });

  await models.User.create({
    name: 'Lauri',
    username: 'lauri',
    password: 'test4',
    cars: [{
      make: 'Mercedes',
      model: 'C250',
      colour: 'Silver',
    }],
  }, {
    include: [models.Car],
  });

  await models.User.create({
    name: 'Alisa',
    username: 'alisa',
    password: 'test5',
  });

  await models.User.create({
    name: 'Karoliina',
    username: 'karoliina',
    password: 'test6',
    cars: [{
      make: 'Volvo',
      model: 'XC90',
      colour: 'White',
    }, {
      make: 'Imaginary',
      model: 'Unicorn',
      colour: 'Rainbow',
    }],
  }, {
    include: [models.Car],
  });
};

sequelize.sync({ force: true }).then(async () => {
  try {
    await createData();
    process.exit();
  } catch (e) {
    console.error(e);
  }
});
